<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use Yii;

//TODO:Сделать роль админа - нужно для добавления отдела и компании юзерам

class User extends ActiveRecord implements IdentityInterface
{

    public static function tableName()
    {
        return '{{users}}';
    }

    public function setPassword($password)
    {
        $this->password = sha1($password);
    }

    public function validatePassword($password)
    {
        return $this->password === sha1($password);
    }

    //=============================================
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public function getId()
    {
        return $this->id;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    { }

    public function getAuthKey()
    { }

    public function validateAuthKey($authKey)
    { }

    public  function  getUsername() 
    { 
        return $this->surname.' '.$this->name.' '.$this->last_name; 
    }    


    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }

    public function getCoworkers(){
        return Yii::$app->db->createCommand(
            "SELECT
                id,
                CONCAT(surname , ' ', name, ' ', last_name) AS name
            FROM
                users
            WHERE
                department_id = $this->department_id
            AND id != $this->id"
        )->queryAll();
    }

    public function getHrs(){
        $user_department = $this->department;
        return Yii::$app->db->createCommand(
            "SELECT
                u.id,
                CONCAT(surname , ' ', u.name, ' ', last_name) AS name
            FROM
                users u
            LEFT JOIN departments d ON
                (d.id = u.department_id)
            WHERE
                d.name = 'HR' AND d.company_id =(
                SELECT
                    d.company_id
                FROM
                    users u
                LEFT JOIN departments d ON
                    (d.id = u.department_id)
                WHERE
                    d.id = $this->department_id
                LIMIT 1
            ) AND u.id != $this->id
            AND u.id != $user_department->head_user_id
            "
        )->queryAll();
    }

    public function getLeftDays()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }
}
