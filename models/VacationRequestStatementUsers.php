<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacation_request_statement_users".
 *
 * @property int $id
 * @property int $user_id
 * @property int $vacation_requests_id
 * @property int $department_id
 * @property string $status
 *
 * @property Comments[] $comments
 * @property VacationRequests $vacationRequests
 * @property User $user
 * @property Departments $department
 */
class VacationRequestStatementUsers extends \yii\db\ActiveRecord
{

    public $coworker_user_id;
    public $hr_user_id;
    public $department_head_user_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacation_request_statement_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'vacation_requests_id', 'department_id'], 'integer'],
            [['status'], 'string'],
            [['role'], 'string'],
            [['vacation_requests_id'], 'exist', 'skipOnError' => true, 'targetClass' => VacationRequests::className(), 'targetAttribute' => ['vacation_requests_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departments::className(), 'targetAttribute' => ['department_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Утверждающий пользователь',
            'vacation_requests_id' => 'Заявка',
            'department_id' => 'Отдел',
            'status' => 'Статус',
            'role' => 'Роль ответственного',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['vacation_request_statement_users_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacationRequests()
    {
        return $this->hasOne(VacationRequests::className(), ['id' => 'vacation_requests_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getStatusName(){
        switch($this->status){
            case 'new':
                return 'Новая';
            break;
            case 'approved':
                return 'Подтверждена';
            break;
            case 'rejected':
                return 'Отклонена';
            break;
        }
    }

    public function getRoleName(){
        switch($this->role){
            case 'coworker':
                return 'Временно исполняющий обязанности';
            break;
            case 'hr':
                return 'Представитель HR отдела';
            break;
            case 'head':
                return 'Глава отдела';
            break;
        }
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }
}
