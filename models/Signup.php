<?php

namespace app\models;

use yii\base\Model;

class Signup extends Model
{
    public $email;
    public $password;
    public $name;
    public $surname;
    public $last_name;
    public $department_id;

    public function rules()
    {
        return [
            [['name', 'surname', 'last_name', 'email', 'department_id' ,'password'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\User'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departments::className(), 'targetAttribute' => ['department_id' => 'id']],
            ['password', 'string', 'min' => 2, 'max' => 10]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'last_name' => 'Отчество',
            'email' => 'E-mail',
            'password' => 'Пароль',
            'department_id'=>'Компания: отдел'
        ];
    }

    public function signup()
    {
        $user = new User();
        $user->email = $this->email;
        $user->name = $this->name;
        $user->surname = $this->surname;
        $user->last_name = $this->last_name;
        $user->department_id = $this->department_id;
        $user->setPassword($this->password);
        return $user->save(); //вернет true или false
    }
}
