<?php

namespace app\models;

use Yii;

use yii\base\Model;

/**
 * This is the model class for table "vacation_requests".
 *
 * @property int $id
 * @property int $user_id
 * @property int $days
 * @property string $vacation_pay_date
 *
 * @property Users $user
 * @property vacationRequestStatementUsers $vacationRequestStatementUsers
 */
class VacationRequests extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacation_requests';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'days', 'start_date', 'vacation_pay_date'], 'required'],
            [['user_id', 'days'], 'integer'],
            [['user_id','days'], 'checkdays'],
            [['start_date','vacation_pay_date'], 'validate_vacation_date'],
            [['vacation_pay_date', 'start_date'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    //Валидация не раньше чем завтрашний день для дат в заявке 
    public function validate_vacation_date($attribute, $params)
    {
        $datetime = new \DateTime('tomorrow');
        $tomorrow = $datetime->format('Y-m-d');

        if (strtotime($this->start_date)<strtotime($tomorrow)) {
            $this->addError('start_date', "Некорректная дата начала отпуска (не раньше чем $tomorrow)");
        }
        if (strtotime($this->vacation_pay_date)<strtotime($tomorrow)) {
            $this->addError('vacation_pay_date', "Некорректная дата выплаты отпускных (не раньше чем $tomorrow)");
        }
    }

    //Валидация количества дней отпуска 
    public function checkdays($attribute, $params)
    {
        $user_id = $this->user->id;
        
        $users_days_left = Yii::$app->db->createCommand(
            "SELECT days_left FROM `users_days_left` where user_id = $user_id"
        )->queryOne();

        $days_left = $users_days_left['days_left'];

        if ($this->days > $days_left) {
            $this->addError('days', "У вас есть только $days_left дней отпуска");
        } else if ($this->days < 1) {
            $this->addError('days', 'Количество дней не может быть меньше 1');
        }
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер заявки',
            'user_id' => 'Пользователь',
            'days' => 'Количество дней',
            'vacation_pay_date' => 'День выплаты',
            'start_date' => 'Начало отпуска',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getStatus()
    {
        $VacationRequestStatements =  VacationRequestStatementUsers::find()
        ->where(['vacation_requests_id' => $this->id])
        ->all();
        //$result = '';
        foreach($VacationRequestStatements as $vrs){
            $result[$vrs->role] = $vrs->statusName;
        }
        return $result;
    }
}
