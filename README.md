COMPANY VACATION REQUESTS SYSTEM

## Start
 1. Clone this repo into a new directory.
 2. Create database and import file `company.sql`
 3. Edit the file `config/db.php` with real data.
 4. Run `composer install` 

CONFIGURATION
-------------

### Database

*all users passwords in `company.sql` is `12341234`

`config/db.php` example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=company',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```
