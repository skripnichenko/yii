<?php

namespace app\controllers;

use Yii;
use app\models\VacationRequests;
use app\models\User;
use app\models\VacationRequestStatementUsers;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * VrController implements the CRUD actions for VacationRequests model.
 */
class VacationRequestsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        return parent::beforeAction($action);
    }


    /**
     * Lists all VacationRequests models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => VacationRequests::find()->where(['user_id' => Yii::$app->user->id]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRequests()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => VacationRequests::find()->where(['user_id' => Yii::$app->user->id]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VacationRequests model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $statementsDataProvider = new ActiveDataProvider([
            'query' => VacationRequestStatementUsers::find()
            ->where(['vacation_requests_id' => $id])
        ]);

        return $this->render('view', [
            'model' => $model,
            'statementsDataProvider' => $statementsDataProvider
        ]);
    }

    /**
     * Creates a new VacationRequests model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new VacationRequests();
        $vr_statement_users_model = new VacationRequestStatementUsers();

        $user_id = Yii::$app->user->id;
        $user_department = Yii::$app->user->identity->department;
        $model->user_id = $user_id;

        //выбираются данные для селекта
        $department_headuser_arr[] = $user_department->headUser->toArray();
        $department_headuser_arr[0]['name'] = $department_headuser_arr[0]['surname'] . ' ' . $department_headuser_arr[0]['name'] . ' ' . $department_headuser_arr[0]['last_name'];
        $coworkers = Yii::$app->user->identity->coworkers;
        $hrs = Yii::$app->user->identity->hrs;

        //форматирую данные для селекта
        $coworkers = ArrayHelper::map($coworkers, 'id', 'name');
        $hrs = ArrayHelper::map($hrs, 'id', 'name');
        $headuser = ArrayHelper::map($department_headuser_arr, 'id', 'name');

        if ($model->load(Yii::$app->request->post())) {

            $post = Yii::$app->request->post();

            $coworker = User::findOne($post['VacationRequestStatementUsers']['coworker_user_id']);
            $hr = User::findOne($post['VacationRequestStatementUsers']['hr_user_id']);
            $department_head = User::findOne($post['VacationRequestStatementUsers']['department_head_user_id']);

            //TODO: По идее это можно вынести в отдельную модель которая вмещает в себе все 3 VacationRequestStatementUsers
            //создаются заявки на утверждение для каждого подтверждающего
            $VacationRequestStatementUsers_coworker = new VacationRequestStatementUsers();
            $VacationRequestStatementUsers_hr = new VacationRequestStatementUsers();
            $VacationRequestStatementUsers_department_head = new VacationRequestStatementUsers();

            $VacationRequestStatementUsers_coworker->role = 'coworker';
            $VacationRequestStatementUsers_coworker->user_id = $coworker->id;
            $VacationRequestStatementUsers_coworker->department_id = $coworker->department_id;

            $VacationRequestStatementUsers_hr->role = 'hr';
            $VacationRequestStatementUsers_hr->user_id = $hr->id;
            $VacationRequestStatementUsers_hr->department_id = $hr->department_id;

            $VacationRequestStatementUsers_department_head->role = 'head';
            $VacationRequestStatementUsers_department_head->user_id = $department_head->id;
            $VacationRequestStatementUsers_department_head->department_id = $department_head->department_id;

            if ($model->save()) {
                //сохраняю каждую заявку на утверждение
                $VacationRequestStatementUsers_coworker->vacation_requests_id = $model->id;
                $VacationRequestStatementUsers_coworker->save();

                $VacationRequestStatementUsers_hr->vacation_requests_id = $model->id;
                $VacationRequestStatementUsers_hr->save();

                $VacationRequestStatementUsers_department_head->vacation_requests_id = $model->id;
                $VacationRequestStatementUsers_department_head->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'coworkers' => $coworkers,
            'hrs' => $hrs,
            'headuser' => $headuser,
            'model' => $model,
            'vr_statement_users_model' => $vr_statement_users_model,
        ]);
    }

    /**
     * Updates an existing VacationRequests model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $vr_statement_users_model = new VacationRequestStatementUsers();
        $user_department = Yii::$app->user->identity->department;

        //выбираются данные для селекта
        $department_headuser_arr[] = $user_department->headUser->toArray();
        $department_headuser_arr[0]['name'] = $department_headuser_arr[0]['surname'] . ' ' . $department_headuser_arr[0]['name'] . ' ' . $department_headuser_arr[0]['last_name'];
        $coworkers = Yii::$app->user->identity->coworkers;
        $hrs = Yii::$app->user->identity->hrs;

        //форматирую для селекта
        $coworkers = ArrayHelper::map($coworkers, 'id', 'name');
        $hrs = ArrayHelper::map($hrs, 'id', 'name');
        $headuser = ArrayHelper::map($department_headuser_arr, 'id', 'name');

        //получение заявок на утверждение для заявки на отпуск
        $VacationRequestStatements = $vr_statement_users_model->find()
        ->where(['vacation_requests_id' => $id])
        ->all();

        foreach($VacationRequestStatements as $vrs){
            if($vrs->status != 'new'){
                //если кто-то уже отметился то редиректим
                return $this->redirect(['view', 'id' => $model->id]);
            }
            //расставляем выбранных ответсвтенных в свойства модели для отображения
            switch ($vrs->role){
                case 'hr':
                    $vr_statement_users_model->hr_user_id = $vrs->user_id;
                break;
                case 'coworker':
                    $vr_statement_users_model->coworker_user_id = $vrs->user_id;
                break;
                case 'head':
                    $vr_statement_users_model->department_head_user_id = $vrs->user_id;
                break;
            }
        }        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $post = Yii::$app->request->post();

            $coworker = User::findOne($post['VacationRequestStatementUsers']['coworker_user_id']);
            $hr = User::findOne($post['VacationRequestStatementUsers']['hr_user_id']);
            $department_head = User::findOne($post['VacationRequestStatementUsers']['department_head_user_id']);

            //расставляем измененных юзеров и сохраняем записи
            foreach($VacationRequestStatements as $vrs){
                switch ($vrs->role){
                    case 'hr':
                        $vrs->user_id = $hr->id;
                    break;
                    case 'coworker':
                        $vrs->user_id = $coworker->id;
                    break;
                    case 'head':
                        $vrs->user_id = $department_head->id;
                    break;
                }
                $vrs->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'coworkers' => $coworkers,
            'hrs' => $hrs,
            'headuser' => $headuser,
            'model' => $model,
            'vr_statement_users_model' => $vr_statement_users_model,
        ]);
    }

    /**
     * Deletes an existing VacationRequests model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $VacationRequestStatements =  VacationRequestStatementUsers::find()
        ->where(['vacation_requests_id' => $id])
        ->all();

        //не даем удалить если кто-то менял - редиректим
        foreach($VacationRequestStatements as $vrs){
            if($vrs->status != 'new'){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VacationRequests model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VacationRequests the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VacationRequests::findOne($id)) !== null) {
            //если тебя не отмечали в этой записи то редиректим домой
            if($model->user_id != Yii::$app->user->id){
                $this->goHome(); 
            }
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
