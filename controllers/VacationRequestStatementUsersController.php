<?php

namespace app\controllers;

use Yii;
use app\models\VacationRequestStatementUsers;
use app\models\VacationRequestStatementUsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VacationRequestStatementUsersController implements the CRUD actions for VacationRequestStatementUsers model.
 */
class VacationRequestStatementUsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all VacationRequestStatementUsers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VacationRequestStatementUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VacationRequestStatementUsers model.
     * @param integer $id
     * @param integer $user_id
     * @param integer $vacation_requests_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $user_id, $vacation_requests_id)
    {
        $model = $this->findModel($id, $user_id, $vacation_requests_id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing VacationRequestStatementUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $user_id
     * @param integer $vacation_requests_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $user_id, $vacation_requests_id)
    {
        $model = $this->findModel($id, $user_id, $vacation_requests_id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'user_id' => $model->user_id, 'vacation_requests_id' => $model->vacation_requests_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing VacationRequestStatementUsers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $user_id
     * @param integer $vacation_requests_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $user_id, $vacation_requests_id)
    {
        $this->findModel($id, $user_id, $vacation_requests_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VacationRequestStatementUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $user_id
     * @param integer $vacation_requests_id
     * @return VacationRequestStatementUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $user_id, $vacation_requests_id)
    {
        if (($model = VacationRequestStatementUsers::findOne(['id' => $id, 'user_id' => $user_id, 'vacation_requests_id' => $vacation_requests_id])) !== null) {
            //если тебя не отмечали - отправляем домой
            if($model->user_id != Yii::$app->user->id){
                $this->goHome(); 
            }
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
