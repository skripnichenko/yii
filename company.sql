-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 24 2019 г., 16:44
-- Версия сервера: 5.7.25
-- Версия PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `company`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `entity` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `entityId` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `level` smallint(6) NOT NULL DEFAULT '1',
  `createdBy` int(11) NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `relatedTo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `createdAt` int(11) NOT NULL,
  `updatedAt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `entity`, `entityId`, `content`, `parentId`, `level`, `createdBy`, `updatedBy`, `relatedTo`, `url`, `status`, `createdAt`, `updatedAt`) VALUES
(1, '057d15bd', 33, 'Если успеешь сдать проект то одобряю ', NULL, 1, 9, 9, 'User Гапоненко Олег Олегович commented on the page /index.php?r=vacation-requests%2Fview&id=33', '/index.php?r=vacation-requests%2Fview&id=33', 1, 1574511525, 1574511525),
(2, '057d15bd', 32, 'Отвечу за день до указанной даты отпуска', NULL, 1, 9, 9, 'User Гапоненко Олег Олегович commented on the page /index.php?r=vacation-request-statement-users%2Fview&id=31&user_id=9&vacation_requests_id=32', '/index.php?r=vacation-request-statement-users%2Fview&id=31&user_id=9&vacation_requests_id=32', 1, 1574512927, 1574512927),
(3, '057d15bd', 31, 'Подойти к HR', NULL, 1, 9, 9, 'User Гапоненко Олег Олегович commented on the page /index.php?r=vacation-request-statement-users%2Fupdate&id=28&user_id=9&vacation_requests_id=31', '/index.php?r=vacation-request-statement-users%2Fupdate&id=28&user_id=9&vacation_requests_id=31', 1, 1574513680, 1574513680);

-- --------------------------------------------------------

--
-- Структура таблицы `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `companies`
--

INSERT INTO `companies` (`id`, `name`) VALUES
(3, 'ООО Агро'),
(4, 'ЧП А-центр');

-- --------------------------------------------------------

--
-- Структура таблицы `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `company_id` int(11) NOT NULL,
  `head_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `departments`
--

INSERT INTO `departments` (`id`, `name`, `company_id`, `head_user_id`) VALUES
(1, 'HR', 3, 7),
(2, 'Программисты', 3, 8),
(3, 'HR', 4, 11),
(4, 'Программисты', 4, 9),
(5, 'Бухгалтеры', 3, 14),
(6, 'Бухгалтеры', 4, 14);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1574509888),
('m010101_100001_init_comment', 1574509908),
('m160629_121330_add_relatedTo_column_to_comment', 1574509909),
('m161109_092304_rename_comment_table', 1574509909),
('m161114_094902_add_url_column_to_comment_table', 1574509910);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `department_id`, `surname`, `name`, `last_name`, `email`, `password`) VALUES
(7, 2, 'Скрипниченко', 'Владислав', 'Юриевич', 'Vladmoto2009@gmail.com', 'c129b324aee662b04eccf68babba85851346dff9'),
(8, 1, 'Кондратенко', 'Владислав', 'Дмитриевич', '4@gmail.com', 'c129b324aee662b04eccf68babba85851346dff9'),
(9, 1, 'Гапоненко', 'Олег', 'Олегович', '5@gmail.com', 'c129b324aee662b04eccf68babba85851346dff9'),
(10, 1, 'Портянский', 'Юрий', 'Дмитриевич', '6@gmail.com', 'c129b324aee662b04eccf68babba85851346dff9'),
(11, 1, 'Капустянка', 'Дмитрий', 'Анатолиевич', '7@gmail.com', 'c129b324aee662b04eccf68babba85851346dff9'),
(12, 6, 'Юрченко', 'Дмитрий', 'Дмитриевич', '1@gmail.com', 'c129b324aee662b04eccf68babba85851346dff9'),
(13, 2, 'Филаренко', 'Юрий', 'Юриевич', '2@gmail.com', 'c129b324aee662b04eccf68babba85851346dff9'),
(14, 4, 'Мистюкович', 'Юрий', 'Юриевич', '3@gmail.com', 'c129b324aee662b04eccf68babba85851346dff9');

-- --------------------------------------------------------

--
-- Структура таблицы `users_days_left`
--

CREATE TABLE `users_days_left` (
  `user_id` int(11) NOT NULL,
  `days_left` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users_days_left`
--

INSERT INTO `users_days_left` (`user_id`, `days_left`) VALUES
(7, 30),
(8, 30),
(9, 30),
(10, 30),
(11, 30),
(12, 30),
(13, 30),
(14, 30);

-- --------------------------------------------------------

--
-- Структура таблицы `vacation_requests`
--

CREATE TABLE `vacation_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `vacation_pay_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `vacation_requests`
--

INSERT INTO `vacation_requests` (`id`, `user_id`, `days`, `start_date`, `vacation_pay_date`) VALUES
(31, 7, 2, '2019-11-24', '2019-11-24'),
(32, 7, 3, '2019-11-24', '2019-11-24'),
(34, 9, 2, '2019-11-24', '2019-11-24'),
(35, 7, 2, '2019-11-24', '2019-11-24'),
(36, 7, 5, '2019-11-25', '2019-11-30');

-- --------------------------------------------------------

--
-- Структура таблицы `vacation_request_statement_users`
--

CREATE TABLE `vacation_request_statement_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `vacation_requests_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) DEFAULT '0',
  `status` enum('new','approved','rejected') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `role` enum('coworker','hr','head') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `vacation_request_statement_users`
--

INSERT INTO `vacation_request_statement_users` (`id`, `user_id`, `vacation_requests_id`, `department_id`, `status`, `role`) VALUES
(27, 13, 31, 2, 'new', 'coworker'),
(28, 10, 31, 1, 'new', 'hr'),
(29, 8, 31, 1, 'new', 'head'),
(30, 13, 32, 2, 'new', 'coworker'),
(31, 9, 32, 1, 'rejected', 'hr'),
(32, 8, 32, 1, 'new', 'head'),
(36, 8, 34, 1, 'new', 'coworker'),
(37, 8, 34, 1, 'new', 'hr'),
(38, 7, 34, 2, 'approved', 'head'),
(39, 13, 35, 2, 'new', 'coworker'),
(40, 11, 35, 1, 'new', 'hr'),
(41, 8, 35, 1, 'new', 'head'),
(42, 13, 36, 2, 'new', 'coworker'),
(43, 11, 36, 1, 'new', 'hr'),
(44, 8, 36, 1, 'new', 'head');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-Comment-entity` (`entity`),
  ADD KEY `idx-Comment-status` (`status`);

--
-- Индексы таблицы `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `head_employee_id` (`head_user_id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `department_id` (`department_id`);

--
-- Индексы таблицы `users_days_left`
--
ALTER TABLE `users_days_left`
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Индексы таблицы `vacation_requests`
--
ALTER TABLE `vacation_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `vacation_request_statement_users`
--
ALTER TABLE `vacation_request_statement_users`
  ADD PRIMARY KEY (`id`,`user_id`,`vacation_requests_id`),
  ADD KEY `vacation_requests_id` (`vacation_requests_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `department_id` (`department_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `vacation_requests`
--
ALTER TABLE `vacation_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT для таблицы `vacation_request_statement_users`
--
ALTER TABLE `vacation_request_statement_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `departments_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `departments_ibfk_2` FOREIGN KEY (`head_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users_days_left`
--
ALTER TABLE `users_days_left`
  ADD CONSTRAINT `users_days_left_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `vacation_requests`
--
ALTER TABLE `vacation_requests`
  ADD CONSTRAINT `vacation_requests_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `vacation_request_statement_users`
--
ALTER TABLE `vacation_request_statement_users`
  ADD CONSTRAINT `vacation_request_statement_users_ibfk_1` FOREIGN KEY (`vacation_requests_id`) REFERENCES `vacation_requests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vacation_request_statement_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vacation_request_statement_users_ibfk_3` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
