<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VacationRequestStatementUsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки на подтверждение отпускной';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacation-request-statement-users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'an_attributeid',
                'label' => 'ФИО',
                'value' => function($model) { return $model->vacationRequests->user->surname  . " " . $model->vacationRequests->user->name." ".$model->vacationRequests->user->last_name ;},
            ],
    
            'vacationRequests.days',
            'vacationRequests.start_date',
            'vacationRequests.vacation_pay_date',
            [
                'attribute' => 'an_attributeid',
                'label' => 'Отдел',
                'value' => 'vacationRequests.user.department.name'
            ],
            [
                'attribute' => 'an_attributeid',
                'label' => 'Отмечен как',
                'value' => function($model) { 
                    return $model->roleName;
                },
            ],   
            [
                'attribute' => 'an_attributeid',
                'label' => 'Статус',
                'value' => function($model) { 
                    return $model->statusName;
                },
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{view} {update}'],
           
        ],
    ]); ?>


</div>
