<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\VacationRequestStatementUsers */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявка на подтверждение отпускной', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vacation-request-statement-users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id, 'user_id' => $model->user_id, 'vacation_requests_id' => $model->vacation_requests_id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'an_attributeid',
                'label' => 'ФИО',
                'value' => function($model) { return $model->vacationRequests->user->surname  . " " . $model->vacationRequests->user->name." ".$model->vacationRequests->user->last_name ;},
            ],
            'vacationRequests.days',
            'vacationRequests.start_date',
            'vacationRequests.vacation_pay_date',
            [
                'attribute' => 'an_attributeid',
                'label' => 'Отдел',
                'value' => function($model) { return $model->vacationRequests->user->department->name;},
            ],
            [
                'attribute' => 'an_attributeid',
                'label' => 'Статус',
                'value' => function($model) { 
                    return $model->statusName;
                },
            ],
        ],
    ]) ?>


    <?= $this->render('//comments/plugin', [
        'model' => $model->vacationRequests,
    ]) ?>

</div>
