<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VacationRequestStatementUsers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacation-request-statement-users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->hiddenInput(['value'=> $value])->label(false); ?>

    <?= $form->field($model, 'vacation_requests_id')->hiddenInput(['value'=> $value])->label(false); ?>

    <?= $form->field($model, 'department_id')->hiddenInput(['value'=> $value])->label(false); ?>

    <?= $form->field($model, 'status')->dropDownList([ 'approved' => 'Подтвердить', 'rejected' => 'Отклонить', ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
