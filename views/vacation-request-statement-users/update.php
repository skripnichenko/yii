<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VacationRequestStatementUsers */

$this->title = 'Обновить заявку на подтверждение отпускной: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на подтверждение отпускной', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'user_id' => $model->user_id, 'vacation_requests_id' => $model->vacation_requests_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vacation-request-statement-users-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?= $this->render('//comments/plugin', [
        'model' => $model->vacationRequests,
    ]) ?>

</div>
