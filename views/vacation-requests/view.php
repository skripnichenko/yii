<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\VacationRequests */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на отпуск', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vacation-requests-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'days',
            'start_date',
            'vacation_pay_date',
        ],
    ]) ?>

<h3><?= Html::encode('Ответы на заявку') ?></h3>

<?= GridView::widget([
        'dataProvider' => $statementsDataProvider,
        'summary' => false,
        'columns' => [
            [
                'attribute' => 'an_attributeid',
                'label' => 'Ответственный',
                'value' => 'user.username'
            ],
            [
                'attribute' => 'an_attributeid',
                'label' => 'Отдел',
                'value' => 'user.department.name'
            ],
            [
                'attribute' => 'an_attributeid',
                'label' => 'Роль',
                'value' => function($model) { 
                    return $model->roleName;
                },
            ],   
            [
                'attribute' => 'an_attributeid',
                'label' => 'Статус',
                'value' => function($model) { 
                    return $model->statusName;
                },
            ],   
        ],
    ]); ?>

    <?= $this->render('//comments/plugin', [
        'model' => $model,
    ]) ?>

</div>
