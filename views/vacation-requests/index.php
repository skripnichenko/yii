<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки на отпуск';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacation-requests-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать заявку на отпуск', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'an_attributeid',
                'label' => 'ФИО',
                'value' => function($model) { return $model->user->surname  . " " . $model->user->name." ".$model->user->last_name ;},
            ],
            'days',
            'start_date',
            'vacation_pay_date',
            [
                'attribute' => 'an_attributeid',
                'label' => 'Временно исполняющий обязанности',
                'value' => function($model) { return $model->status['coworker'];},
            ],
            [
                'attribute' => 'an_attributeid',
                'label' => 'Сотрудник отдела HR',
                'value' => function($model) { return $model->status['hr'];},
            ],
            [
                'attribute' => 'an_attributeid',
                'label' => 'Глава отдела',
                'value' => function($model) { return $model->status['head'];},
             ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
