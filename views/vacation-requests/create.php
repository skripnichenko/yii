<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VacationRequests */

$this->title = 'Create Заявки на отпуск';
$this->params['breadcrumbs'][] = ['label' => 'Заявки на отпуск', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacation-requests-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'coworkers' => $coworkers,
        'vr_statement_users_model' => $vr_statement_users_model,
        'hrs'=>$hrs,
        'headuser' => $headuser,
    ]) ?>

</div>
