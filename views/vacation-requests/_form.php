<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VacationRequests */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacation-requests-form">

    <?php 
    
    $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'days')->textInput() ?>

    <?= $form->field($model, 'start_date')->widget(\yii\jui\DatePicker::class, [
    'clientOptions' =>[
        'minDate' => '+1d',
    ],
    'dateFormat' => 'yyyy-MM-dd',
]) ?> 

    <?= $form->field($model, 'vacation_pay_date')->widget(\yii\jui\DatePicker::class, [
    'clientOptions' =>[
        'minDate' => '+1d',
    ],
    'dateFormat' => 'yyyy-MM-dd',
]) ?> 

    <?=$form->field($vr_statement_users_model,'coworker_user_id')->dropDownList($coworkers)->label('Временно исполняющий обязанности');
    ?>
    
    <?=$form->field($vr_statement_users_model,'hr_user_id')->dropDownList($hrs)->label('Ответсвенный HR');
    ?>

    <?=$form->field($vr_statement_users_model,'department_head_user_id')->dropDownList($headuser)->label('Глава отдела');;
    ?>
   

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
