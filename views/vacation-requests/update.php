<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VacationRequests */

$this->title = 'Обновить заявку на отпуск: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на отпуск', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vacation-requests-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'coworkers' => $coworkers,
        'vr_statement_users_model' => $vr_statement_users_model,
        'hrs'=>$hrs,
        'headuser' => $headuser,
    ]) ?>

    <?= $this->render('//comments/plugin', [
        'model' => $model,
    ]) ?>

</div>
