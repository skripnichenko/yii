<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yii2mod\comments\widgets\Comment;

// так выглядит плагин комментариев (есть в зависимостях и уст. через комозер)
echo Comment::widget([
    'model' => $model,
    'relatedTo' => 'User ' . Yii::$app->user->identity->username . ' commented on the page ' . \yii\helpers\Url::current(),
    'maxLevel' => 2,
    'dataProviderConfig' => [
        'pagination' => [
            'pageSize' => 10
        ],
    ],
    'listViewConfig' => [
        'emptyText' => \Yii::t('app', 'Нет комментариев'),
    ],
]); 
?>